import "../../node_modules/antd/dist/antd.css";
import "../../node_modules/@ant-design/flowchart/dist/index.css";
import "../../node_modules/antd/es/popover/style/index.css";
import "./global.scss";

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
