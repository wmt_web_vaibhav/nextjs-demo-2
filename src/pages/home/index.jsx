import {
  CaretDownOutlined,
  CheckCircleFilled,
  CloseOutlined,
  ControlOutlined,
  DownCircleOutlined,
  FileDoneOutlined,
  FileOutlined,
  FileTextOutlined,
  FilterOutlined,
  MailOutlined,
  MenuOutlined,
  MessageOutlined,
  PlusOutlined,
  SearchOutlined,
  SmileFilled,
  TagOutlined,
  UsergroupAddOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Affix,
  Button,
  Checkbox,
  Col,
  DatePicker,
  Divider,
  Dropdown,
  Input,
  Menu,
  Row,
  Space,
  Tabs,
  Typography,
} from "antd";
import { useRouter } from "next/router";
import React from "react";
import styles from "./index.module.scss";
const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

const menu = (
  <Menu
    items={[
      {
        label: "1st menu item",
        key: "1",
      },
      {
        label: "2nd menu item",
        key: "2",
      },
      {
        label: "3rd menu item",
        key: "3",
      },
    ]}
  />
);

function index() {
  const router = useRouter();

  const onChange = (key) => {
    console.log(key);
  };

  return (
    <div>
      <Affix offsetTop={0}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            height: "74px",
            alignItems: "center",
            backgroundColor: "#082140",
            padding: "0px 20px",
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <Typography
              style={{
                fontSize: "1.5rem",
                fontWeight: "bold",
                color: "#2e496b",
              }}
            >
              LOGO
            </Typography>
            <Typography
              style={{ fontWeight: "bold", color: "white", marginLeft: "40px" }}
            >
              4219, 4223, 4227 Jakes Landing, montgonery,TX, 77351
            </Typography>
          </div>

          <div style={{ display: "flex", alignItems: "center" }}>
            <Button
              style={{
                color: "#082140",
                fontWeight: "bold",
                height: "45px",
                borderRadius: "5px",
                marginRight: "30px",
                border: "none",
              }}
            >
              V2
            </Button>
            <CloseOutlined
              onClick={() => router.push("/")}
              style={{ color: "white", fontSize: "18px", fontWeight: "bold" }}
            />
          </div>
        </div>
      </Affix>
      <div className={styles.first}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            margin: "0px 50px 0px 10px",
          }}
        >
          <Typography>
            <span className={styles.filt}>Agent: </span>
            <a>Chad Hill</a>
          </Typography>
          <Typography>
            <span className={styles.filt}>Team: </span>
            <a>Florida Blue</a>
          </Typography>
          <Typography>
            <span className={styles.filt}>Brokerage: </span>
            <a>Keller Williams Realty DFW...</a>
          </Typography>
          <Typography>
            <span className={styles.filt}>Empower User: </span>
            <a>Admin Test</a>
          </Typography>
          <Typography>
            <span className={styles.filt}>Trans Email: </span>
            <a>tx+13201-Ranch-Road-6...</a>
          </Typography>
          <Typography>
            <span className={styles.filt}>Closing date: </span>
            <a>12/08/2021</a>
          </Typography>
        </div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Button
            style={{
              color: "#082140",
              borderRadius: "4px",
              marginRight: "10px",
              height: "36px",
              width: "36px",
            }}
            shape="circle"
          >
            <FilterOutlined />
          </Button>
        </div>
      </div>

      <div className={styles.second}>
        <Tabs
          defaultActiveKey="1"
          onChange={onChange}
          style={{ padding: "20px" }}
        >
          <TabPane tab="Details & Date" key="1">
            <div>
              <Tabs
                defaultActiveKey="1"
                onChange={onChange}
                style={{ padding: "0px 20px" }}
              >
                <TabPane tab="Listing Statuses" key="1">
                  <div>
                    <Divider style={{ margin: "5px" }} />
                    <Row>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Contract Status
                        </Typography>
                        <Typography className={styles.listing2}>
                          CTC-Pending
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Contract Client Type
                        </Typography>
                        <Typography className={styles.listing2}>
                          Buyer
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Billing Name if Different from Agent
                        </Typography>
                        <Typography className={styles.listing2}>
                          Not Set
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Empower TC
                        </Typography>
                        <a
                          style={{
                            marginTop: "10px",
                            fontSize: "13px",
                            fontWeight: "bold",
                          }}
                        >
                          Jessica Laforce
                        </a>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Address
                        </Typography>
                        <Typography className={styles.listing2}>
                          15227 Beacham Drive
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          City
                        </Typography>
                        <Typography className={styles.listing2}>
                          Houston
                        </Typography>
                      </Col>
                    </Row>
                    <Divider style={{ margin: "10px" }} />
                    <Row>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          State
                        </Typography>
                        <Typography className={styles.listing2}>TX</Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>Zip</Typography>
                        <Typography className={styles.listing2}>
                          77070
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Country
                        </Typography>
                        <Typography className={styles.listing2}>
                          Haris
                        </Typography>
                      </Col>
                      <Col sm={4}>
                        <Typography className={styles.listing1}>
                          Property Type
                        </Typography>
                        <Typography className={styles.listing2}>
                          Residential
                        </Typography>
                      </Col>
                    </Row>
                    <Divider style={{ margin: "10px" }} />
                  </div>
                </TabPane>
                <TabPane tab="Transaction Dates" key="2">
                  Content of Transaction Dates
                </TabPane>
                <TabPane tab="Transaction Details" key="3">
                  Content of Transaction Details
                </TabPane>
              </Tabs>
            </div>
          </TabPane>
          <TabPane tab="Empower Tracking-Enter '1' in all that apply" key="2">
            Content of Empower Tracking-Enter '1' in all that apply
          </TabPane>
          <TabPane tab="Agent information" key="3">
            Content of Agent information
          </TabPane>
          <TabPane tab="Do not Use" key="4">
            Content of Do not Use
          </TabPane>
        </Tabs>
        <Tabs
          defaultActiveKey="1"
          onChange={onChange}
          style={{ padding: "0px 20px" }}
        >
          <TabPane
            tab={
              <span>
                <FileDoneOutlined />
                Tasks
              </span>
            }
            key="1"
          >
            <div>
              <Tabs defaultActiveKey="1" onChange={onChange}>
                <TabPane tab="Active" key="1">
                  <div>
                    <div className={styles.first1}>
                      <div>
                        <Dropdown overlay={menu}>
                          <a onClick={(e) => e.preventDefault()}>
                            <Space className={styles.default}>
                              <span>All</span>
                              <CaretDownOutlined />
                            </Space>
                          </a>
                        </Dropdown>
                        <RangePicker
                          size="small"
                          style={{
                            width: "210px",
                            height: "32px",
                            marginRight: "10px",
                          }}
                        />
                        <Button
                          icon={<SmileFilled style={{ color: "#ef64b8" }} />}
                          className={styles.button}
                        />
                        <Button
                          icon={<UserOutlined />}
                          className={styles.button}
                        />
                        <Button
                          icon={<TagOutlined />}
                          className={styles.button}
                        />
                        <Button
                          icon={<MenuOutlined />}
                          className={styles.button}
                        />
                      </div>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Input
                          className={styles.search}
                          size="middle"
                          placeholder="Search Tasks"
                          suffix={<SearchOutlined />}
                        />
                        <Button icon={<PlusOutlined />} className={styles.task}>
                          Add Task
                        </Button>
                      </div>
                    </div>
                    <div>
                      <Typography
                        style={{
                          fontSize: "13px",
                          fontWeight: "bold",
                          marginTop: "20px",
                        }}
                      >
                        Pinned Tasks(2)
                      </Typography>
                      <Divider style={{ margin: "10px" }} />
                      {[0, 1].map((data) => {
                        return (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              fontSize: "13px",
                              borderLeft: "5px solid #EF64B8",
                              padding: "5px 0px 5px 10px",
                              borderRadius: "5px",
                              marginBottom: "15px",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                width: "100%",
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  padding: "10px",
                                }}
                              >
                                <DownCircleOutlined
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <CheckCircleFilled
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <Checkbox
                                  style={{
                                    fontSize: "13px",
                                  }}
                                >
                                  <a
                                    style={{
                                      color: "#4191FF",
                                    }}
                                  >
                                    <SmileFilled
                                      style={{
                                        color: "#ef64b8",
                                        marginRight: "8px",
                                      }}
                                    />
                                    Tue Jul, 6th
                                  </a>
                                </Checkbox>
                                <Typography>
                                  Broker: eXp Reality, LLC. Back Office. SkySlop
                                </Typography>
                              </div>
                              <div>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    color: "grey",
                                  }}
                                >
                                  <MailOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <MessageOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <FileTextOutlined
                                    style={{
                                      marginRight: "5px",
                                      backgroundColor: "#E2BD26",
                                      color: "white",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    <div>
                      <Typography
                        style={{
                          fontSize: "13px",
                          fontWeight: "bold",
                          marginTop: "20px",
                        }}
                      >
                        Scheduled Tasks(2)
                      </Typography>
                      <Divider style={{ margin: "10px" }} />
                      {[0, 1].map((data) => {
                        return (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              fontSize: "13px",
                              borderLeft: "5px solid #E2BD26",
                              padding: "5px 0px 5px 10px",
                              borderRadius: "5px",
                              marginBottom: "15px",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                width: "100%",
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  padding: "10px",
                                }}
                              >
                                <DownCircleOutlined
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <CheckCircleFilled
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <Checkbox
                                  style={{
                                    fontSize: "13px",
                                  }}
                                >
                                  <a
                                    style={{
                                      color: "#4191FF",
                                    }}
                                  >
                                    <SmileFilled
                                      style={{
                                        color: "#E2BD26",
                                        marginRight: "8px",
                                      }}
                                    />
                                    Tue Jul, 6th
                                  </a>
                                </Checkbox>
                                <Typography>
                                  Broker: eXp Reality, LLC. Back Office. SkySlop
                                </Typography>
                              </div>
                              <div>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    color: "grey",
                                  }}
                                >
                                  <MailOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <MessageOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <FileTextOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    <div>
                      <Typography
                        style={{
                          fontSize: "13px",
                          fontWeight: "bold",
                          marginTop: "20px",
                        }}
                      >
                        UnScheduled Tasks(2)
                      </Typography>
                      <Divider style={{ margin: "10px" }} />
                      {[0, 1].map((data) => {
                        return (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "flex-start",
                              fontSize: "13px",
                              borderLeft: "5px solid purple",
                              padding: "5px 0px 5px 10px",
                              borderRadius: "5px",
                              marginBottom: "15px",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                width: "100%",
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  padding: "10px",
                                }}
                              >
                                <DownCircleOutlined
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <CheckCircleFilled
                                  style={{
                                    marginRight: "10px",
                                  }}
                                />
                                <Checkbox
                                  style={{
                                    fontSize: "13px",
                                  }}
                                >
                                  <a
                                    style={{
                                      color: "#4191FF",
                                    }}
                                  >
                                    <SmileFilled
                                      style={{
                                        color: "purple",
                                        marginRight: "8px",
                                      }}
                                    />
                                    Tue Jul, 6th
                                  </a>
                                </Checkbox>
                                <Typography>
                                  Broker: eXp Reality, LLC. Back Office. SkySlop
                                </Typography>
                              </div>
                              <div>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    color: "grey",
                                  }}
                                >
                                  <MailOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <MessageOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                  <FileTextOutlined
                                    style={{
                                      marginRight: "5px",
                                      padding: "6px",
                                      borderRadius: "3px",
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </TabPane>
                <TabPane tab="Completed" key="2">
                  Content of Transaction Dates
                </TabPane>
              </Tabs>
            </div>
          </TabPane>
          <TabPane
            tab={
              <span>
                <MailOutlined />
                Email
              </span>
            }
            key="2"
          >
            Content of Empower Tracking-Enter '1' in all that apply
          </TabPane>
          <TabPane
            tab={
              <span>
                <FileOutlined />
                Documents
              </span>
            }
            key="3"
          >
            Content of Agent information
          </TabPane>
          <TabPane
            tab={
              <span>
                <FileTextOutlined />
                Text
              </span>
            }
            key="4"
          >
            Content of Do not Use
          </TabPane>
          <TabPane
            tab={
              <span>
                <UsergroupAddOutlined />
                Contacts & Vendors
              </span>
            }
            key="5"
          >
            Content of Do not Use
          </TabPane>
          <TabPane
            tab={
              <span>
                <FileTextOutlined />
                Notes
              </span>
            }
            key="6"
          >
            Content of Do not Use
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}

export default index;
