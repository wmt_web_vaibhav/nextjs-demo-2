import { Pie } from "@ant-design/plots";
import {
  CaretDownOutlined,
  ControlOutlined,
  DropboxOutlined,
  ExperimentFilled,
  FileImageFilled,
  FilePdfFilled,
  FilterOutlined,
  InfoCircleOutlined,
  NotificationFilled,
  NotificationOutlined,
  SlackOutlined,
} from "@ant-design/icons";
import {
  Badge,
  Button,
  Col,
  Divider,
  Dropdown,
  Menu,
  Progress,
  Row,
  Space,
  Typography,
  DatePicker,
  List,
  Avatar,
} from "antd";
import React, { useState, useEffect } from "react";
import Layout from "../Layouts/index";
import styles from "./index.module.scss";
const { RangePicker } = DatePicker;

function index() {
  const menu = (
    <Menu
      items={[
        {
          label: "1st menu item",
          key: "1",
        },
        {
          label: "2nd menu item",
          key: "2",
        },
        {
          label: "3rd menu item",
          key: "3",
        },
      ]}
    />
  );

  const TotalData = [
    {
      title: "Total Transactions in ETC",
      number: "9963",
      percentage: "80",
      color: "#3C90FF",
    },
    {
      title: "Total Transactions in Yoday",
      number: "63",
      percentage: "63",
      color: "blue",
    },
    {
      title: "Total Closing - July",
      number: "25",
      percentage: "25",
      color: "#e2bb0d",
    },
    {
      title: "Total Closing Today",
      number: "02 ",
      percentage: "10",
      color: "#28db08",
    },
  ];

  const data = [
    {
      type: "Not Started Tasks",
      value: 60,
    },
    {
      type: "Completed Tasks",
      value: 20,
    },
    {
      type: "Past Due Tasks",
      value: 20,
    },
  ];

  const config = {
    appendPadding: 0,
    data,
    angleField: "value",
    colorField: "type",
    color: ["#e2bb0d", "#28db08", "#e54245"],
    radius: 0.8,
    innerRadius: 0.6,
    label: {
      type: "inner",
      offset: "-50%",
      content: "{value}",
      style: {
        textAlign: "center",
        fontSize: 14,
      },
    },
    interactions: [
      {
        type: "element-selected",
      },
      {
        type: "element-active",
      },
    ],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: "pre-wrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
        },
        content: "100",
      },
    },
    legend: {
      label: "{value}",
      offsetX: -75,
    },
  };

  return (
    <Layout>
      <div className={styles.first}>
        <Dropdown overlay={menu}>
          <a onClick={(e) => e.preventDefault()}>
            <Space className={styles.default}>
              <span>Default</span>
              <CaretDownOutlined />
            </Space>
          </a>
        </Dropdown>
        <Button icon={<ControlOutlined />} className={styles.search}>
          <span>Search</span>
          <span className={styles.badge}>8</span>
        </Button>
      </div>
      <div>
        <Row style={{ margin: "8px" }}>
          {TotalData.map((data) => {
            return (
              <Col sm={6} style={{ padding: "8px" }}>
                <div
                  style={{
                    padding: "20px",
                    boxShadow:
                      "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <Typography>
                      {data.title} <InfoCircleOutlined />
                    </Typography>
                    <Badge dot style={{ backgroundColor: "#e2bb0d" }}>
                      <FilterOutlined />
                    </Badge>
                  </div>
                  <Typography style={{ fontSize: "40px", marginTop: "10px" }}>
                    {data.number}
                  </Typography>
                  <Divider style={{ margin: "0px" }} />
                  <Progress
                    style={{ marginTop: "10px" }}
                    trailColor="lightgrey"
                    strokeColor={data.color}
                    percent={data.percentage}
                  />
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
      <div>
        <Row style={{ margin: "8px", height: "300px" }}>
          <Col sm={12} style={{ padding: "8px" }}>
            <div
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                height: "100%",
              }}
            >
              <div
                style={{
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography>
                  Task <InfoCircleOutlined />
                </Typography>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    fontSize: "12px",
                  }}
                >
                  <Typography style={{ marginRight: "10px" }}>Today</Typography>
                  <Typography style={{ marginRight: "10px" }}>
                    This Week
                  </Typography>
                  <Typography style={{ marginRight: "10px" }}>
                    This month
                  </Typography>
                  <RangePicker
                    size="small"
                    style={{ width: "210px", height: "30px" }}
                  />
                </div>
              </div>
              <Divider style={{ margin: "0px" }} />
              <div style={{ marginTop: "10px" }}>
                <div style={{}}>
                  <Pie height={250} width={250} {...config} />
                </div>
              </div>
            </div>
          </Col>
          <Col sm={12} style={{ padding: "8px" }}>
            <div
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                height: "100%",
              }}
            >
              <div
                style={{
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography>
                  Contract Approval <InfoCircleOutlined />
                </Typography>
              </div>
              <Divider style={{ margin: "0px" }} />
              <div
                style={{ margin: "20px", overflow: "auto", height: "235px" }}
              >
                <div>
                  <a href="#" style={{ textDecoration: "none" }}>
                    11408 Parkfield Drive, Austin, TX, 78758
                  </a>
                  <Typography style={{ fontSize: "12px", marginTop: "6px" }}>
                    <span style={{ fontWeight: "bold" }}>Submitted:</span> Tue,
                    Jul 6th @ 12:12 pm IST (20 min ago)
                  </Typography>
                </div>
                <Divider style={{ margin: "10px 0px" }} />
                <div>
                  <a href="#" style={{ textDecoration: "none" }}>
                    11408 Parkfield Drive, Austin, TX, 78758
                  </a>
                  <Typography style={{ fontSize: "12px", marginTop: "6px" }}>
                    <span style={{ fontWeight: "bold" }}>Submitted:</span> Tue,
                    Jul 5th @ 10:00 am IST (1 day ago)
                  </Typography>
                  <Divider style={{ margin: "10px 0px" }} />
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div style={{ marginTop: "40px" }}>
        <Row style={{ margin: "8px", height: "300px" }}>
          <Col sm={12} style={{ padding: "8px" }}>
            <div
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                height: "100%",
              }}
            >
              <div
                style={{
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography>
                  Unapproved Documents <InfoCircleOutlined />
                </Typography>
              </div>
              <Divider style={{ margin: "0px" }} />
              <List
                style={{ margin: "20px", overflow: "auto", height: "235px" }}
              >
                <List.Item
                  actions={[
                    <Button
                      style={{
                        borderRadius: "3px",
                        backgroundColor: "#1dd18f",
                        color: "white",
                      }}
                    >
                      Approve
                    </Button>,
                    <Button style={{ borderRadius: "3px" }}>Reject</Button>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#5310e5" }}
                        shape="circle"
                        icon={<FilePdfFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={<a href="https://ant.design">Survey.pdf</a>}
                    description="11408 Parkfield Drive, Austin, TX, 78758"
                  />
                </List.Item>
                <Divider style={{ margin: "0px" }} />
                <List.Item
                  actions={[
                    <Button
                      style={{
                        borderRadius: "3px",
                        backgroundColor: "#1dd18f",
                        color: "white",
                      }}
                    >
                      Approve
                    </Button>,
                    <Button style={{ borderRadius: "3px" }}>Reject</Button>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#5310e5" }}
                        shape="circle"
                        icon={<FileImageFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={<a href="https://ant.design">File Image.png</a>}
                    description="11408 Parkfield Drive, Austin, TX, 78758"
                  />
                </List.Item>
                <Divider style={{ margin: "0px" }} />
                <List.Item
                  actions={[
                    <Button
                      style={{
                        borderRadius: "3px",
                        backgroundColor: "#1dd18f",
                        color: "white",
                      }}
                    >
                      Approve
                    </Button>,
                    <Button style={{ borderRadius: "3px" }}>Reject</Button>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#5310e5" }}
                        shape="circle"
                        icon={<FilePdfFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={<a href="https://ant.design">New Document.pdf</a>}
                    description="11408 Parkfield Drive, Austin, TX, 78758"
                  />
                </List.Item>
                <Divider style={{ margin: "0px" }} />
                <List.Item
                  actions={[
                    <Button
                      style={{
                        borderRadius: "3px",
                        backgroundColor: "#1dd18f",
                        color: "white",
                      }}
                    >
                      Approve
                    </Button>,
                    <Button style={{ borderRadius: "3px" }}>Reject</Button>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#5310e5" }}
                        shape="circle"
                        icon={<FilePdfFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={<a href="https://ant.design">New Document.pdf</a>}
                    description="11408 Parkfield Drive, Austin, TX, 78758"
                  />
                </List.Item>
              </List>
            </div>
          </Col>
          <Col sm={12} style={{ padding: "8px" }}>
            <div
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                height: "100%",
              }}
            >
              <div
                style={{
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography>
                  Company News <InfoCircleOutlined />
                </Typography>
              </div>
              <Divider style={{ margin: "0px" }} />
              <List
                style={{ margin: "20px", overflow: "auto", height: "235px" }}
              >
                <List.Item>
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#e2bb0d" }}
                        shape="circle"
                        icon={<ExperimentFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={
                      <a href="https://ant.design">Happy BirthDay johan!</a>
                    }
                    description="Here is a wish for your birthday May you receive whatever you ask for, may you find whatever you seek, Happy birthday"
                  />
                </List.Item>
                <Divider style={{ margin: "0px" }} />
                <List.Item>
                  <List.Item.Meta
                    avatar={
                      <Button
                        style={{ backgroundColor: "#e2bb0d" }}
                        shape="circle"
                        icon={<NotificationFilled style={{ color: "white" }} />}
                        size="large"
                      />
                    }
                    title={
                      <a href="https://ant.design">
                        Some Other News/ Announcement
                      </a>
                    }
                    description="Here is a wish for your birthday May you receive whatever you ask for, may you find whatever you seek, Happy birthday, Happy birthday"
                  />
                </List.Item>
              </List>
            </div>
          </Col>
        </Row>
      </div>
      <div style={{ margin: "40px 0px" }}>
        <Row style={{ margin: "8px", height: "300px" }}>
          <Col sm={12} style={{ padding: "8px" }}>
            <div
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                height: "100%",
              }}
            >
              <div
                style={{
                  padding: "10px 20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Typography>
                  Training Updates <InfoCircleOutlined />
                </Typography>
              </div>
              <Divider style={{ margin: "0px" }} />
              <List
                style={{ margin: "20px", overflow: "auto", height: "235px" }}
              >
                <List.Item
                  actions={[
                    <Button type="primary" style={{ borderRadius: "3px" }}>
                      Watch Video
                    </Button>,
                  ]}
                >
                  <List.Item.Meta
                    title="New training Video Title"
                    description="Here is a wish for your birthday May you receive whatever you ask for, may you find whatever you seek, Happy birthday"
                  />
                </List.Item>
                <Divider style={{ margin: "0px" }} />
              </List>
            </div>
          </Col>
          <Col sm={6} style={{ padding: "8px" }}>
            <div className={styles.openapp}>
              <SlackOutlined style={{ fontSize: "64px", color: "#41A9FF" }} />
              <Typography
                style={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  marginTop: "40px",
                }}
              >
                Slack
              </Typography>
              <Typography
                style={{
                  fontSize: "13px",
                  marginTop: "10px",
                  textAlign: "center",
                }}
              >
                Lorep ipsum dolor sit amet,conseteture sadipscing elitr, sed
                siam.
              </Typography>
              <Button className={styles.openappbutton}>Open Slack</Button>
            </div>
          </Col>
          <Col sm={6} style={{ padding: "8px" }}>
            <div className={styles.openapp}>
              <DropboxOutlined style={{ fontSize: "64px", color: "#41A9FF" }} />
              <Typography
                style={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  marginTop: "40px",
                }}
              >
                Dropbox
              </Typography>
              <Typography
                style={{
                  fontSize: "13px",
                  marginTop: "10px",
                  textAlign: "center",
                }}
              >
                Lorep ipsum dolor sit amet,conseteture sadipscing elitr, sed
                siam.
              </Typography>
              <Button className={styles.openappbutton}>Open Dropbox</Button>
            </div>
          </Col>
        </Row>
      </div>
    </Layout>
  );
}

export default index;
