import {
  CaretDownOutlined,
  CheckCircleFilled,
  ControlOutlined,
  FileTextOutlined,
  MailOutlined,
  MenuOutlined,
  MessageOutlined,
  SearchOutlined,
  SmileFilled,
} from "@ant-design/icons";
import {
  Button,
  Checkbox,
  Col,
  Divider,
  Dropdown,
  Input,
  List,
  Menu,
  Row,
  Space,
  Tabs,
  Typography,
} from "antd";
import React, { useState } from "react";
import Layout from "../../Layouts/index";
import styles from "./index.module.scss";
const { TabPane } = Tabs;
function index() {
  const menu = (
    <Menu
      items={[
        {
          label: "1st menu item",
          key: "1",
        },
        {
          label: "2nd menu item",
          key: "2",
        },
        {
          label: "3rd menu item",
          key: "3",
        },
      ]}
    />
  );

  const taskData = [
    {
      title: "Past Due",
      details: [
        {
          date: "11/11/2021",
          title: "7400 peggie Drive,AUSTIN, TX 78724",
          tasks: [
            {
              title: "Tue Jul, 5th",
              desc: "Email Buyer: Home Warranty Selection",
              checked: false,
            },
          ],
        },
        {
          date: "11/11/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "Tue Jul, 1th",
              desc: "Email Buyer: Home Warranty Selection",
              checked: false,
            },
            {
              title: "Tue Jul, 1th",
              desc: "Email Buyer: Home Warranty Selection",
              checked: false,
            },
          ],
        },
      ],
    },
    {
      title: "Today Task",
      details: [
        {
          date: "12/10/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "Buyer Replay with Home Warranty? ADD to Fields",
              checked: false,
            },
          ],
        },
        {
          date: "11/11/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "DA: Send to Title or Confirm Title received from Broker.",
              checked: false,
            },
            {
              title: "30 Day Check-in with Buyer",
              checked: false,
            },
          ],
        },
        {
          date: "12/10/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "Buyer Replay with Home Warranty? ADD to Fields",
              checked: false,
            },
          ],
        },
      ],
    },
    {
      title: "Tomorrow",
      details: [
        {
          date: "12/10/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "Buyer Replay with Home Warranty? ADD to Fields",
              checked: false,
            },
          ],
        },
        {
          date: "11/11/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "DA: Send to Title or Confirm Title received from Broker.",
              checked: false,
            },
            {
              title: "30 Day Check-in with Buyer",
              checked: false,
            },
          ],
        },
        {
          date: "12/10/2021",
          title: "11408 Parkfield Drive,AUSTIN, TX 78758",
          tasks: [
            {
              title: "Buyer Replay with Home Warranty? ADD to Fields",
              checked: false,
            },
          ],
        },
      ],
    },
  ];
  const [checked, setChecked] = useState([]);
  const handleCheckBox = (e, value) => {
    setChecked((check) =>
      e.target.checked
        ? [...check, ...[value]]
        : check.filter((item) => item !== value)
    );
  };

  return (
    <Layout>
      <div className={styles.first}>
        <div>
          <Dropdown overlay={menu}>
            <a onClick={(e) => e.preventDefault()}>
              <Space className={styles.default}>
                <span>Default</span>
                <CaretDownOutlined />
              </Space>
            </a>
          </Dropdown>
          <Dropdown overlay={menu}>
            <a onClick={(e) => e.preventDefault()}>
              <Space className={styles.default}>
                <span>All Roles</span>
                <CaretDownOutlined />
              </Space>
            </a>
          </Dropdown>
          <Dropdown overlay={menu}>
            <a onClick={(e) => e.preventDefault()}>
              <Space className={styles.default}>
                <span>All Users</span>
                <CaretDownOutlined />
              </Space>
            </a>
          </Dropdown>
          <Button
            icon={<SmileFilled style={{ color: "#ef64b8" }} />}
            className={styles.button}
          />
          <Button icon={<MenuOutlined />} className={styles.button} />
        </div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Button
            style={{
              backgroundColor: "#082140",
              color: "white",
              borderRadius: "4px",
              marginRight: "10px",
              height: "36px",
              width: "36px",
            }}
            shape="circle"
          >
            v1
          </Button>
          <Input
            className={styles.search}
            size="middle"
            placeholder="Search Tasks"
            suffix={<SearchOutlined />}
          />
          <Button icon={<ControlOutlined />} className={styles.filter}>
            <span>Filters</span>
            <span className={styles.badge}>3</span>
          </Button>
        </div>
      </div>
      <div>
        <Row style={{ margin: "8px", height: "300px" }}>
          {taskData.map((data) => {
            return (
              <Col sm={8} style={{ padding: "8px" }}>
                <div
                  style={{
                    boxShadow:
                      "rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px",
                    height: "100%",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "20px",
                    }}
                  >
                    <Typography>{data.title}</Typography>
                    <Checkbox onClick={(e) => handleCheckBox(e, data.title)}>
                      Select All
                    </Checkbox>
                  </div>
                  <Tabs style={{ padding: "0px 20px" }} defaultActiveKey="1">
                    <TabPane tab="Active (3)" key="1">
                      <Input
                        style={{
                          borderRadius: "5px",
                          height: "36px",
                          marginBottom: "10px",
                        }}
                        size="middle"
                        placeholder="Search Tasks"
                        suffix={<SearchOutlined />}
                      />
                      {data.details.map((items) => {
                        return (
                          <div style={{ margin: "10px 0px" }}>
                            <Typography
                              style={{
                                fontSize: "12px",
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <span style={{ color: "#2E496B" }}>
                                {items.title}
                              </span>
                              <span style={{ color: "grey" }}>
                                Offer Started: {items.date}
                              </span>
                            </Typography>
                            <Divider style={{ margin: "5px 0px" }} />
                            <div>
                              <List>
                                {items.tasks.map((item) => {
                                  return (
                                    <>
                                      <List.Item
                                        style={{
                                          display: "flex",
                                          flexDirection: "column",
                                          alignItems: "flex-start",
                                          fontSize: "13px",
                                          borderLeft: "5px solid #EF64B8",
                                          borderLeftStyle: "",
                                          padding: "5px 0px 5px 10px",
                                        }}
                                      >
                                        <div
                                          style={{
                                            display: "flex",
                                            justifyContent: "space-between",
                                            width: "100%",
                                          }}
                                        >
                                          <div style={{ display: "flex" }}>
                                            <CheckCircleFilled
                                              style={{
                                                marginRight: "10px",
                                                marginTop: "5px",
                                              }}
                                            />
                                            <Checkbox
                                              style={{
                                                fontSize: "13px",
                                              }}
                                              checked={checked.includes(
                                                data.title
                                              )}
                                            >
                                              <a
                                                style={{
                                                  color:
                                                    data.title !== "Past Due"
                                                      ? "black"
                                                      : "#4191FF",
                                                }}
                                              >
                                                {item.title}
                                              </a>
                                            </Checkbox>
                                          </div>
                                          <div>
                                            <div
                                              style={{
                                                display: "flex",
                                                alignItems: "center",
                                                color: "grey",
                                              }}
                                            >
                                              <MailOutlined
                                                style={{
                                                  marginRight: "5px",
                                                  padding: "6px",
                                                  borderRadius: "3px",
                                                }}
                                              />
                                              <MessageOutlined
                                                style={{
                                                  marginRight: "5px",
                                                  padding: "6px",
                                                  borderRadius: "3px",
                                                }}
                                              />
                                              <FileTextOutlined
                                                style={{
                                                  marginRight: "5px",
                                                  backgroundColor: "#E2BD26",
                                                  color: "white",
                                                  padding: "6px",
                                                  borderRadius: "3px",
                                                }}
                                              />
                                            </div>
                                          </div>
                                        </div>
                                        {item?.desc ? (
                                          <div>
                                            <Typography
                                              style={{
                                                fontWeight: "bold",
                                                marginTop: "10px",
                                              }}
                                            >
                                              {item?.desc}
                                            </Typography>
                                          </div>
                                        ) : null}
                                      </List.Item>
                                      <Divider style={{ margin: "5px 0px" }} />
                                    </>
                                  );
                                })}
                              </List>
                            </div>
                          </div>
                        );
                      })}
                    </TabPane>
                    <TabPane tab="Completed (0)" key="2">
                      Content of Tab Pane 2
                    </TabPane>
                  </Tabs>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
    </Layout>
  );
}

export default index;
