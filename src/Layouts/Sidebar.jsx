import {
  BugOutlined,
  CalendarOutlined,
  DashboardOutlined,
  FileDoneOutlined,
  HomeOutlined,
  SettingOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { Affix, Layout, Menu } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import styles from "./sidebar.module.scss";
const { Header, Content, Footer, Sider } = Layout;

function Sidebar({ children }) {
  const [collapsed, setCollapsed] = useState(true);
  const router = useRouter();

  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Affix offsetTop={74}>
          <Sider
            theme="light"
            collapsible
            collapsed={collapsed}
            onCollapse={(value) => setCollapsed(value)}
          >
            <div className="logo" />
            <Menu
              defaultSelectedKeys={[router.pathname]}
              mode="inline"
              style={{ color: "#082140" }}
            >
              <Menu.Item
                className={styles.menuItem}
                key="/"
                onClick={() => router.push("/")}
              >
                <DashboardOutlined />
                <span>Dashboard</span>
              </Menu.Item>
              <Menu.Item key="/home" onClick={() => router.push("/home")}>
                <HomeOutlined />
                <span>Home</span>
              </Menu.Item>
              <Menu.Item key="/tasks" onClick={() => router.push("/tasks")}>
                <FileDoneOutlined />
                <span>Tasks</span>
              </Menu.Item>
              <Menu.Item key="4">
                <CalendarOutlined />
                <span>Calendar</span>
              </Menu.Item>
              <Menu.Item key="5">
                <TeamOutlined />
                <span>Teams</span>
              </Menu.Item>
              <Menu.Item key="6">
                <BugOutlined />
                <span>Bugs</span>
              </Menu.Item>
              <Menu.Item key="7">
                <SettingOutlined />
                <span>Settings</span>
              </Menu.Item>
            </Menu>
          </Sider>
        </Affix>
        <Layout className="site-layout">
          <Content style={{ margin: "0 0px" }}>{children}</Content>
          <Footer style={{ textAlign: "center" }}>
            Copyright © EmpowerTC {new Date().getFullYear()}
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
}

export default Sidebar;
