import { BellOutlined, SearchOutlined } from "@ant-design/icons";
import { Affix, Avatar, Badge, Button, Input, Typography } from "antd";
import React from "react";
import styles from "./header.module.css";

function Header() {
  return (
    <Affix offsetTop={0}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: "74px",
          alignItems: "center",
          backgroundColor: "#082140",
          padding: "0px 20px",
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <Typography className={styles.logo}>LOGO</Typography>
          <Typography
            style={{ fontWeight: "bold", color: "white", marginLeft: "40px" }}
          >
            Dashboard
          </Typography>
        </div>
        <div>
          <Input
            size="large"
            placeholder="Search Transactions, Agents, Teams, Brokers, Contacts"
            suffix={<SearchOutlined style={{ color: "white" }} />}
            style={{
              backgroundColor: "#2e496b",
              border: "none",
              width: "420px",
              borderRadius: "5px",
            }}
            className={styles.searchInput}
          />
        </div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Button
            style={{
              backgroundColor: "#1dd18f",
              color: "white",
              fontWeight: "bold",
              height: "45px",
              borderRadius: "5px",
              marginRight: "20px",
              border: "none",
            }}
          >
            + Add
          </Button>
          <Typography style={{ marginRight: "20px" }}>
            <Badge dot>
              <BellOutlined
                style={{
                  fontSize: "20px",
                  color: "white",
                  fontWeight: "bold",
                }}
              />
            </Badge>
          </Typography>
          <Avatar shape="circle" size="large" style={{}} />
        </div>
      </div>
    </Affix>
  );
}

export default Header;
