import React from "react";
import Header from "./Header";
import Sidebar from "./Sidebar";

function index({ children }) {
  return (
    <div>
      <Header />
      <Sidebar children={children} />
    </div>
  );
}

export default index;
